#pragma once

class Animal
{
    int age;
    int poid;

    public:
    Animal(int age, int poid) : age(age), poid(poid){}

    void afficher() const;

    virtual void emettreSon()=0;
    
};