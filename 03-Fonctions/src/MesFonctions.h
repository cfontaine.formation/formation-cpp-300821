// Fichier .h => contient la déclaration des fonctions

#ifndef MES_FONCTIONS_H // Indique au compilateur de n'intègrer le fichier d’en-tête qu’une seule fois, lors de la compilation d’un fichier de code source
#define MES_FONCTIONS_H
// ou #pragma once

#include <cstdarg>
#include <iostream>

// déclaration d'une variable qui a été définie dans main.cpp. Il n' y a pas d'allocation mémoire
extern const  int ctsGlobal;

//extern double varGlobal;  // varGlobal est static dans main.cpp, on ne peut l'utiliser que dans ce fichier

// Passage de paramètre par valeur
// On peut définir des valeurs par défaut pour les paramètres
// Ils doivent se trouver en fin de liste des arguments
// On place les valeurs par défaut dans la déclaration des fonctions
void testParam(int, int m = 12);

// Passage de paramètre par adresse
// En utilisant le passage de paramètres par adresse, on modifie réellement la variable qui est passée en paramètre
void testAdresse(int* ptr);
void testAdresse2(int** ptr);

// ERREUR => fonctions qui retournent une référence ou un pointeur sur une variable locale
int* testRetourPointeur();

// Passage de paramètre par référence
// Comme avec  le passage de paramètres par adresse, on modifie réellement la variable qui est passée en paramè
void testreference(int& ref);
void testreference2(const int& refCst = 1);
bool estPresent(const char* tabChr, int size, char chr);

void afficherTableau(int tab[], int size);
void calculTableau(int* t, int size, int& min, int& max, double& moy);
void afficherMenu();
void menu();

// Fonction à nombre variable de paramètre  (hérité du C)
double moyenne(int nbArgs, ...);

// Fonction a nombre variable de paramètre  (C++11)
double moyenneCpp11(std::initializer_list<int> varg);

// Récursivité
int factorial(int n);

// Pointeur de fonction
typedef double (*ptr_fonction)(double, double);
double somme(double, double);
double multiplier(double, double);
void afficherOperation(double, double, ptr_fonction);

// Test de classe de mémorisation
void testMemorisation();
void testExtern();

std::string inverser(const std::string& str);
bool palindrome(const std::string& str);
std::string acronyme(const std::string& str);

#endif