#pragma once
#include "Animal.h"
class Refuge
{
    Animal* place[10];
    int placeOccuper;

    public:
    Refuge(): placeOccuper(0)
    {
        for(int i=0;i<10;i++)
        {
            place[i]=nullptr;
        }
    }

    ~Refuge();
    
    void ajouter(Animal * animal);

    void ecouter();

};