#pragma once

class Test
{
    int data;

    public:
    Test (int data): data(data){   }


    void afficher();

    friend void fonctionAmie(Test &t);
    friend class ClasseAmie;

};