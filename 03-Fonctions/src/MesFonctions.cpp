#include "MesFonctions.h"

using namespace std;

void testParam(int n, int r)
{
	cout << r << n << endl;
}

void  testAdresse(int* ptr)
{
	*ptr = 123;
}

void  testAdresse2(int** ptr)
{
	*ptr = new int(12);
}

void testreference(int& ref)
{
	ref = 123;
}

void testreference2(const int& refCst)
{
	cout << refCst << endl;
	//refCst=123;
}

int* testRetourPointeur()
{
	return new int(12);
	//
	// int res=456;
	//return &res;
}
bool estPresent(const char* tabChr, int size, char chr)
{
	for (int i = 0; i < size; i++)
	{
		if (tabChr[i] == chr)
		{
			return true;
		}
	}
	return false;
}

void afficherTableau(int tab[], int size) {
	cout << "[ ";
	for (int i = 0; i < size; i++)
	{
		cout << " " << tab[i];
	}
	cout << "]" << endl;
}

int* saisirTableau(int& size) {
	cout << "Saisir la taille du tableau= ";
	cin >> size;
	int* t = nullptr;
	if (size > 0)
	{
		t = new int[size];
		for (int i = 0; i < size; i++)
		{
			cout << "t[" << i << "]=";
			cin >> t[i];
		}
	}
	return t;
}


void calculTableau(int* t, int size, int& min, int& max, double& moy)
{
	max = t[0];
	min = t[0];
	double somme = 0;
	for (int i = 0; i < size; i++)
	{
		if (t[i] > max)
		{
			max = t[i];
		}
		if (t[i] < min)
		{
			min = t[i];
		}
		somme += t[i];
	}
	moy = somme / size;
}

void afficherMenu()
{
	cout << "1 - Saisir le tableau" << endl;
	cout << "2 - Afficher le tableau" << endl;
	cout << "3 - Afficher le minimum, le maximum et la moyenne" << endl;
	cout << "0 - Quitter" << endl;
}

void menu()
{
	int* tab = nullptr;
	int size = 0;
	int choix;
	do
	{
		afficherMenu();
		cout << "Choix= ";
		cin >> choix;
		switch (choix)
		{
		case 1:
			delete[] tab;
			tab = saisirTableau(size);
			break;

		case 2:
			if (tab != 0)
			{
				afficherTableau(tab, size);
			}
			else
			{
				cout << "Il n'y a pas de tableau saisie" << endl;
			}
			break;

		case 3:
			if (tab != 0)
			{
				int min, max;
				double moy;
				calculTableau(tab, size, min, max, moy);
				cout << "minimum= " << min << " maximum=" << max << " moyenne=" << moy << endl;
			}
			else
			{
				cout << "Il n'y a pas de tableau saisie" << endl;
			}

			break;
		case 0:
			cout << "Au revoir!" << endl;
			break;
		default:
			cout << "choix non disponnible" << endl;
		}
	} while (choix != 0);
	delete[] tab;
}

double moyenne(int nbArgs, ...)
{
	va_list valist;
	double somme = 0.0;
	va_start(valist, nbArgs);
	for (int i = 0; i < nbArgs; i++)
	{
		somme += va_arg(valist, int);
	}
	va_end(valist); // nettoyage des arguments
	return somme / nbArgs;
}

double moyenneCpp11(initializer_list<int> varg)
{
	double somme = 0.0;
	for (auto v : varg) {
		somme += v;
	}
	if (varg.size() != 0)
	{
		return somme / varg.size();
	}
	return 0.0;
}



int factorial(int n) // factoriel= 1* 2* … n
{
	if (n <= 1) // condition de sortie
	{
		return 1;
	}
	else
	{
		return factorial(n - 1) * n;
	}
}

double somme(double a, double b)
{
	return a + b;
}

double multiplier(double a, double b)
{
	return a * b;
}

void afficherOperation(double a, double b, ptr_fonction fonction) {
	cout << fonction(a, b) << endl;
}

void testMemorisation()
{
	static int stat = 10;
	cout << "variable static" << stat << endl;
	stat++;
}

void testExtern()
{
	std::cout << ctsGlobal << std::endl;
	//   std::cout<<varGlobal<<std::endl;
}

string inverser(const string& str)
{
	string tmp = "";
	for (auto it = str.rbegin(); it != str.rend(); it++)
	{
		tmp.push_back(*it);
	}
	return tmp;
}

bool palindrome(const string& str)
{
	return str == inverser(str);
}

string acronyme(const string& str)
{
	string acr = "";
	auto itDebutMot = str.begin();
	for (auto it = str.begin(); it != str.end(); it++)
	{
		if (*it == ' ' || *it == '\'' || it == str.end() - 1)
		{
			if (it - itDebutMot > 2)
			{
				acr.push_back(*itDebutMot);
			}
			itDebutMot = it + 1;
		}
	}
	return acr;
}
