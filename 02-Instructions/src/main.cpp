#include <iostream>
using namespace std;


// énumération en C++98
enum Direction{NORD=90,SUD=270,EST=0,OUEST=180};

// énumération en c++11
enum class Motorisation{ELECTRIQUE,DIESEL=20,ESSENCE,GPL};

int main()
{
	// Condition if
	int v;
	cin >> v;
	if (v > 100)
	{
		cout << "v>100" << endl;
	}
	else if (v < 100)
	{
		cout << "v<100" << endl;
	}
	else
	{
		cout << "v==100" << endl;
	}

	int a, b;
	cin >> a >> b;
	int c = a + b;
	cout << a << " + " << b << " = " << c << endl;

	// Exercice: Moyenne
	// Saisir 2 nombres entiers et afficher la moyenne dans la console
	int a1, a2;
	cin >> a1 >> a2;
	double d = (a1 + a2) / 2.0;
	cout << "moyenne=" << d;

	// Exercice: Parité
	// Créer un programme qui indique, si le nombre entier saisie dans la console est paire ou impaire
	int val;
	cin >> val;
	if ((val & 1) == 0)	// ou if(val%2==0)
	{
		cout << "Paire" << endl;
	}
	else
	{
		cout << "Impaire" << endl;
	}

	// Exercice: Intervalle
	// Saisir un nombre et dire s'il fait parti de l'intervalle - 4 (exclus)et 7 (inclus)
	int vi;
	cin >> vi;
	if (vi > -4 && vi <= 7) {
		cout << vi << " fait parti de l'intervalle" << endl;
	}

	// Condition: switch
	const int G = 1;
	int jours = 7;
	switch (jours)
	{
	case G:
		std::cout << "Lundi" << std::endl;
        break;
	case G + 5:
	case G + 6:
		std::cout << "Week end !" << std::endl;
        break;
	default:
		std::cout << "Un autre jour" << std::endl;
        break;
	}

	// Exercice : Calculatrice
	// Faire un programme calculatrice
	//	Saisir dans la console
	//	- un double
	//	- une caractère opérateur qui a pour valeur valide : + - * /
	//	- un double

	// Afficher:
	// - Le résultat de l’opération
	// - Une message d’erreur si l’opérateur est incorrecte
	// - Une message d’erreur si l’on fait une division par 0
	double va1, va2;
	char op;
	cin >> va1 >> op >> va2;
	switch (op)
	{
	case '+':
		cout << " = " << va1 + va2 << endl;
		break;
	case '-':
		cout << " = " << va1 - va2 << endl;
		break;
	case '*':
		cout << " = " << va1 * va2 << endl;
		break;
	case '/':
		if (va2 == 0.)
		{
			cout << "Division par 0";
		}
		else
		{
			cout << " = " << va1 / va2 << endl;
		}
		break;
	default:
		cout << op << " l'opérateur est inconnue" << endl;
	}

	// Condition: opérateur ternaire
	// Valeur absolue
	// Saisir un nombre et afficher la valeur absolue sous la forme | -1.85 | = 1.85
	double abs;
	cin >> abs;
	double r = abs >= 0.0 ? abs : -abs;
	cout << "|" << abs << "| = " << r << endl;

	// Boucle: while
	int k = 0;
	while (k < 10)
	{
		cout << k << endl;
		k++;
	}

	// Boucle: do  while
	do {
		cout << k << endl;
		k++;
	} while (k < 20);

	// Boucle: for
	for (int i = 10; i > 0; i--)
	{
		cout << "i=" << i << endl;
	}

	// Instructions de branchement
	// break
	for (int i = 0; i < 10; i++)
	{
		cout << "i=" << i << endl;
		if (i == 2)
		{
			break;	// break => termine la boucle
		}
	}

	// continue
	for (int i = 0; i < 10; i++)
	{
		if (i == 2)
		{
			continue;	// continue => on passe à l'itération suivante
		}
		cout << "i=" << i << endl;
	}

	// goto
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			cout << "i=" << i << "j=" << j << endl;
			if (i == 1)
			{
				goto EXIT_LOOP;	// Utilisation de goto pour sortir de 2 boucles imbriquée
			}
		}

	}
EXIT_LOOP:

	// Exercice : Table de multiplication
	// Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
	//	1 X 4 = 4
	//	2 X 4 = 8
	//	…
	//	9 x 4 = 36
	for (;;)
	{
		int v2;
		cin >> v2;
		if (v2 < 1 || v2 > 9)
		{
			break;
		}
		for (int i = 1; i < 10; i++)
		{
			cout << i << " x " << v2 << "=" << i * v2 << endl;
		}
	}

	// Tableau à une dimension
	// Déclaration: type nom[taille]
	int tab[5];

	// Initialisation du tableau
	for (int i = 0; i < 5; i++) {
		tab[i] = 0;
	}

	// Accès à un élément du tableau
	tab[1] = 3;
	cout << "tab[1]=" << tab[1] << endl;

	// Déclaration et initialisation d'un tableau
	char tabChr[] = { 'a','z','e' };

	// Parcourir un tableau
	for (int i = 0; i < 3; i++)
	{
		cout << tabChr[i] << endl;
	}

	// Parcourir un tableau en C++11
	// Pas de modification des éléments du tableau
	for (char c : tabChr)
	{
		cout << c << endl;
		c = '1';
	}

	//  En utilisant une référence, on peut  modifier des éléments du tableau
	for (char& c : tabChr)
	{
		cout << c << endl;
		c = '1';
	}

	// Calculer la taille d'un tableau ( /!\ ne fonctionne pas avec les tableaux passés en paramètre de fonction)
	cout << "nombre d'octet du tableau=" << sizeof(tab) << "nb element" << sizeof(tab) / sizeof(tab[0]) << endl;

	// Exercice : tableau
	// Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers: -7, 4, 8, 0, -3
	int t[] = { -7, 4, 8, 0, -3 };
	int max = t[0];
	double somme = 0.0;
	for (int i = 0; i < 5; i++)
	{
		if (t[i] > max)
		{
			max = t[i];
		}
		somme += t[i];
	}
	cout << "maximum=" << max << " moyenne=" << somme / 5 << endl;

	// Tableau à 2 dimensions
	// Déclaration
	double tab2D[3][4];

	// Initialisation du tableau
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			tab2D[i][j] = 0.0;
		}
	}

	// Accès à un élément
	tab2D[0][2] = 4.5;

	// Déclaration et initialisation
	int tab2di[3][2] = { {1,3},{4,5},{3,7} };

	// Parcourir le tableau
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			cout << tab2D[i][j] << "\t";
		}
		cout << endl;
	}

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			cout << tab2di[i][j] << "\t";
		}
		cout << endl;
	}

	// Pointeur
	double valeur=1.5;

	// Déclaration d'un pointeur de double
	double *ptr;
	
	// &valeur => adresse de la variable valeur
	cout<<"Adresse de valeur="<<&valeur<<endl;
	ptr=&valeur;

	// *ptr => Accès au contenu de la variable pointer par ptr
	cout<<ptr<< " "<<*ptr<<endl;
	*ptr=2.0;

	cout<<"valeur="<<valeur<<endl;

	// Pointeur de pointeur de double
	double** ptrptr=&ptr;
	cout<< ptrptr <<" "<<&ptr<<endl;
	cout<<*ptrptr<<endl; 
	cout<<**ptrptr<<endl; 

	// Un pointeur qui ne pointe sur rien
	// double* ptr2=0; //  0 en c++98  (correspond à NULL en C)
	double* ptr2=nullptr;	// nullptr en C++11

	//  On peut affecter un pointeur avec un pointeur du même type
	ptr2=ptr;
	cout<<ptr2<<endl;

	// On peut comparer 2 pointeurs du même type ou compare un pointeur à 0 ou nullptr
	if(ptr==ptr2){
		cout<<"Les pointeurs sont égaux"<<endl;
	}

	if(ptr==nullptr){
		cout<<"Le pointeur est égal à null"<<endl;
	}

	// Pointeur constant
	// En préfixant un pointeur avec const => Le contenu de la variable pointée est constant
	int valInt=42;
	const int* ptrCst= &valInt;
	cout<<*ptrCst<<endl;
	//*ptrCst=1;	// on ne peut plus modifier le contenu pointé par l'intermédiaire du pointeur
	ptrCst=nullptr;	// Mais on peut modifier le pointeur

	// En postfixant un pointeur avec const => le pointeur est constant
	int* const ptrCstPost=&valInt;
	cout<<*ptrCstPost<<endl;
	*ptrCstPost=100;
	//ptrCstPost=nullptr;	 // Le pointeur ptrCstPost  est constant, on ne peut plus le modifier


	// En préfixant et en postfixant un pointeur avec const: le pointeur ptrCst2 est constant et
	// l'on ne peut plus modifier le contenu de la variable pointée par l'intermédiaire du pointeur
	const int* const ptrCst2=&valInt;
	cout<<*ptrCst2<<endl;
	//*ptrCst2=1;
	//ptrCst2=nullptr;

	// Opérateur const_cast
	double valCst=3.4;
	const double* ptrCst3=&valCst;
	//double* ptrValCst=(double*)ptrCst3;	// en C
	double* ptrValCst=const_cast<double*>(ptrCst3);	// en C++, const_cast permet de supprimer le qualificatif const
	*ptrValCst=12.34;
	cout<<valCst<<endl;

	// Exercice Pointeur
	double v1 = 10.5, v2 = 20.6, v3 = 30.7;
	double* ptrChoix = 0;
	int choix;
	cin >> choix;
	switch (choix)
	{
	case 1:
		ptrChoix = &v1;
		break;
	case 2:
		ptrChoix = &v2;
		break;
	case 3:
		ptrChoix = &v3;
		break;
	default:
		cout << "erreur Choix " << endl;
	}
	if (ptrChoix != nullptr) {
		cout << *ptrChoix << endl;
	}

	// Tableau et Pointeur
	int tab3[]= {40,50,60,70};
	int *ptrTab3=tab3;		// Le nom d'un tableau est un pointeur sur le premère élément
	cout << *ptrTab3 << endl;	// On accède au premier élément du tableau

	// On accède au deuxième élément du tableau
	cout<<*(ptrTab3+1)<<endl;	// équivalent à ptrTab + sizeof(int) ou à tab3[1]
	cout<<ptrTab3[1]<<endl; 	// on peut accèder à un tableau avec un pointeur comme à un tableau statique avec l'opérateur []

	ptrTab3++;
	cout<<ptrTab3[1]<<endl; // 60

	//reinterpret_cast
	int str=0x61| 0x68 <<8 | 0X62 <<16; // 00 62 68 61
	cout<<str<<hex<<str<<endl;
	int* ptrStr=&str;
	char *ptrChr=reinterpret_cast<char*>(ptrStr);
	cout<<*ptrChr<<endl;	// a
	cout<<ptrChr[1]<<endl;	// h
	cout<<ptrChr[2]<<endl;	// b

	// Exercice Tableau et Pointeur
	double tp[] = { 2.5,5.3,8.6,9.7,23.0 };
	int choix2 = 0;
	cin >> choix2;
	if (choix2 > 0 && choix2 <= 5) {
		double* ptrChoix2 = tp + choix2 - 1; // &tp[choix2-1]
		cout << *ptrChoix2 << endl;
	}
	else
	{
		cout << "erreur" << endl;
	}
	
	// Allocation de mémoire dynamique
	int* ptrDyn= new int;	// new => allocation de la mémoire
	*ptrDyn=12;
	cout<<*ptrDyn<<endl;
	cout<<ptrDyn<<endl;
	delete ptrDyn;			// delete => libération de la mémoire
	cout<<ptrDyn<<endl;
	ptrDyn=nullptr;

	int tsti=34;
	ptrDyn=&tsti;
	delete ptrDyn;

	// Allocation dynamique avec initialisation
	ptrDyn= new int(12);	// en C++ 98
	cout<<*ptrDyn<<endl;
	delete ptrDyn;

	ptrDyn= new int{12};	// en C++11
	cout<<*ptrDyn<<endl;
	delete ptrDyn;

	// Allocation dynamique d'un tableau
	char* ptrstrDyn= new char[10];
	*ptrstrDyn='a'; 	// ptrstrDyn[0]='a'
	delete[] ptrstrDyn;	// delete [] => libération d'un tableau dynamique


	// Référence 
	// Une référence correspond à un autre nom que l'on donne à la variable
	// on doit aussi intialiser une référence avec une lvalue (= qui possède une adresse)
	int k6=123;
	int &ref=k6;
	ref=456;
	cout<< k6 <<" "<<ref<<endl;
	//int &ref2=345;	// Erreur pas d'initialisation avec une littéralle

	// Référence constante
	const int & refCst=k6;
	// refCst=123456;		// On ne peut plus utiliser une référence constante pour modifier la valeur
	const int & refCst2=688;	// On peut initialiser une référence constante avec une valeur litérale
	cout<< refCst <<" " << refCst2<<endl;

	// énumération C++ 98
	Direction dir=SUD; // Direction::SUD
	// enum -> int: Conversion implicite
	int valDir=dir;
	cout<<valDir<<endl;

	// int-> enum à éviter
	// dir=(Direction)16;

	// énumération C++ 11
	Motorisation mo=Motorisation::DIESEL;
	// enum -> int: convertion explicite
	int valMo=static_cast<int>(mo);
	cout<<valMo<<endl;

	// énumération et switch
	switch(mo)
	{
		case Motorisation::DIESEL:
			cout<<"voiture Diesel"<<endl;
		break;
		default:
			cout<<"Autre motorisation"<<endl;
	}

	return 0;
}