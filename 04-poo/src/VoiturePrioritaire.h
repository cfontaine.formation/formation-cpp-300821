#pragma once

#include "Voiture.h"
#include <iostream>
#include <string>

class VoiturePrioritaire : public Voiture
{
    bool gyro=false;

public:
    VoiturePrioritaire()
    {
        std::cout<<"Constructeur par défaut VoiturePrioritaire"<<std::endl;
    }

    VoiturePrioritaire(bool gyro) : gyro(gyro)
    {
            std::cout<<"Constructeur VoiturePrioritaire"<<std::endl;
    }

    VoiturePrioritaire(std::string marque,  std::string couleur, std::string plaqueIma,bool gyro) : Voiture(marque,couleur,plaqueIma), gyro(gyro)
    {
            std::cout<<"Constructeur VoiturePrioritaire 4 parametres"<<std::endl;
    }

    void allumerGyro();
    void eteindreGyro();
    void afficher() const;
};