#include<iostream>
#include "Voiture.h"
#include "CompteBancaire.h"
#include "CompteEpargne.h"
#include "VoiturePrioritaire.h"
#include <memory>

using namespace std;

int main ()
{ 
    Voiture::testVarClasse();
    cout<<"Nombre d'objet voiture cree="<<Voiture::getCompteurVoiture()<<endl;//Voiture::compteurVoiture
    Voiture v1;
    cout<<"Nombre d'objet voiture cree="<<Voiture::getCompteurVoiture()<<endl;
    //v1.vitesse=10;
    v1.accelerer(20);
    v1.afficher();
    v1.freiner(5);
    v1.afficher();
    cout<<v1.estArreter()<<endl;
    v1.arreter();
    cout<<v1.estArreter()<<endl;

    Voiture *v2=new Voiture;
   // v2->vitesse=10;

    cout<< v1.getVitesse()<< " "<<v2->getVitesse()<<endl;
     delete v2;

    Voiture v3("Honda","Noir","AF-1234");
    v3.afficher();

    Voiture *v4=new Voiture("Honda","Noir","AF-1234",40,100);
    v4->afficher();

    Voiture v5=v1;
    v5.afficher();
    cout<<"Nombre d'objet voiture cree="<<Voiture::getCompteurVoiture()<<endl;

    //cout<<Voiture::egalVitesse(v1,v3)<<endl;

    const Voiture v6("fiat","jaune","fr-45Y7");
    v6.afficher();
   // v6.couleur="Vert";
   // v6.afficher();

    
/*
    // Pointeurs intelligents
    unique_ptr<int> uniPtr; // nullptr

    unique_ptr<int> uniPtr1(new int(12)); // {new int(12)}
    cout<<*uniPtr1<< " "<< uniPtr1.get() <<endl;

    unique_ptr<int> uniPtr2{new int(25)};
    cout<<*uniPtr2<< " "<< uniPtr2.get() <<endl;
    //uniPtr2=uniPtr1;
    uniPtr2=std::move(uniPtr1); // uniPtr1 nullptr 
    cout<<*uniPtr2<< " "<< uniPtr2.get() <<endl;

    // C++14
    unique_ptr<int> uniPtr3=std::make_unique<int>(1); 

    //shared_ptr<int> shrptr1(new int(23));
    shared_ptr<int> shrptr1;
    shrptr1=move(uniPtr2);
    shared_ptr<int> shrptr2(shrptr1);
    shared_ptr<int> shrptr3;
    shrptr3=shrptr1;
    cout<<*shrptr1<<" "<<shrptr1.get()<<"  "<<shrptr1.use_count()<<endl;
    shrptr3=nullptr;
    cout<<*shrptr1<<" "<<shrptr1.get()<<"  "<<shrptr1.use_count()<<endl;

    unique_ptr<Voiture> uvPtr(new Voiture("toyota","blanc","az-1233-ze"));
    unique_ptr<Voiture> uvPtr2=make_unique<Voiture>("renault","blanc","az-1233-ze");
    uvPtr->afficher();
    unique_ptr<Voiture> uvPtr3=move(uvPtr2);
    uvPtr3->afficher();

    shared_ptr<Voiture> svPtr(new Voiture("honda","noir","az-1233-ze"));
    shared_ptr<Voiture> svPtr1(svPtr);
    shared_ptr<Voiture> svPtr2(make_shared<Voiture>("ford","bleu","az-1233-ze"));

    svPtr->afficher();
    svPtr1->afficher();

    svPtr2->afficher();
    cout<<svPtr.use_count()<< " "<<svPtr2.use_count()<<endl;

    svPtr2=move(uvPtr3);
    svPtr2->afficher();
    cout<<"---------------------"<<endl;
   */

    // Compte Bancaire
    shared_ptr<Adresse> adr1=make_shared<Adresse>("1, rue Esquermoise","Lille","59000");
    shared_ptr<Personne> per1 (make_shared<Personne>("john","doe",adr1));
    
    CompteBancaire cb (200.0,per1);
    cb.afficher();
    cb.crediter(500.0);
    cb.debiter(40.0);
    cb.afficher();
    shared_ptr<Personne> per2 (make_shared<Personne>("jane","doe",adr1));
    
    CompteBancaire cb2 (1000.0,per2);
    cb2.afficher();



    CompteBancaire cb3 (1000.0,(make_shared<Personne>("Alan","Smithee",make_shared<Adresse>("32 Boulevard Vincent Gache","Nantes","44000"))));
    cb3.afficher();
    
   
    CompteEpargne ce1(per1,1000.0,5.5);
    ce1.afficher();
    ce1.calculInterets();
    ce1.afficher();
    
    
    
    // Agrégation
    // Personne* per1=new Personne("John","Doe");
    //shared_ptr<Personne> per1 (make_shared<Personne>("john","doe"));
    v4->setProprietaire(per1);
    Voiture *v7=new Voiture("Ford" ,"rouge", "az-1245-be",per1);
    v1.afficher();
    v4->afficher();
    v7->afficher();

    Voiture v8=*v4;
    delete v4;
    v8.afficher();
    delete v7; 
    //delete per1;

    // Héritage

    VoiturePrioritaire vp1;
    vp1.accelerer(10);
    vp1.afficher();

    VoiturePrioritaire vp2("ford","jaune","www-wwww",true);
    vp2.accelerer(10);
    vp2.afficher();



    return 0;
}