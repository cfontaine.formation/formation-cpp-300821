#pragma once
#include <string>
#include <memory>
#include "Personne.h"

class CompteBancaire
{

	protected:
	double solde=0.0;
	private:
	std::string iban;
	std::shared_ptr<Personne> titulaire;

	static int cptCompte;

    public:
	CompteBancaire(std::shared_ptr<Personne> titulaire) : titulaire(titulaire)
	{
		genererIban();
	}

	CompteBancaire(double solde,std::shared_ptr<Personne> titulaire ) : solde(solde),titulaire(titulaire)
	{
		genererIban();
	}

    //CompteBancaire(CompteBancaire &c)=delete;


	std::string getIban() const
	{
		return iban;
	}

	double getSolde() const
	{
		return solde;
	}

	std::shared_ptr<Personne> getTitulaire() const
	{
		return titulaire;
	}

	void setTitulaire(std::shared_ptr<Personne> titulaire)
	{
		this->titulaire=titulaire;
	}
	
    void afficher() const;

	void crediter(int valeurDepot);

	void debiter(int valeurDebit);

	bool estPositif() const ;

	void genererIban();

    //static void afficher(std::initializer_list<CompteBancaire> comptes);

};
