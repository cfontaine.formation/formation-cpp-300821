#include "Refuge.h"

void Refuge::ajouter(Animal * animal)
{
    if(placeOccuper<10)
    {
        place[placeOccuper]=animal;
        placeOccuper++;
    }
}

void Refuge::ecouter()
{
    for(auto a : place)
    {
        if(a)
        {
            a->emettreSon();
        }
    }
}

Refuge::~Refuge()
{
    for(int i=0;i<10;i++)
    {
        delete place[i];
    }
}