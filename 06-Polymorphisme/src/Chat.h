#pragma once

#include "Animal.h"
#include <string>

class Chat : public Animal
{
    int nbVie=9;
    public:
    Chat(int age,int poid, int nbVie) : Animal(age,poid), nbVie(nbVie){}

    int getNbVie() const
    {
        return nbVie;
    }

    void emettreSon();
};