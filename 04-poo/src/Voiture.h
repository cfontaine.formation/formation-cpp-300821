#pragma once
#include <string>
#include <iostream>
#include <memory>
#include "Personne.h"
#include "Moteur.h"
class Voiture
{
    
    std::string marque;
    mutable std::string couleur;
    std::string plaqueIma;
    int vitesse =0; // C++11
    int compteurKm = 20;

    // Agrégation
    //Personne *proprietaire=nullptr;
    std::shared_ptr<Personne> proprietaire;

    // Composition 
    //Moteur *moteur;
    std::unique_ptr<Moteur> moteur;

    // Variable de classe
    static int compteurVoiture;
public:
  //  Voiture(){} // c++98
    // Voiture() = default;
    Voiture()
    {
        moteur=std::make_unique<Moteur>();
        compteurVoiture++;
        std::cout<<"Constructeur par défaut de voiture"<<std::endl;
    }

    Voiture(std::string marque,  std::string couleur, std::string plaqueIma) : marque(marque),couleur(couleur),plaqueIma(plaqueIma)
    {        
        compteurVoiture++;
        moteur=std::make_unique<Moteur>();
        std::cout<<"Constructeur 3 arguments de Voiture"<<std::endl;
    }

    Voiture(std::string marque,  std::string couleur, std::string plaqueIma,int vitesse) : Voiture(marque,couleur,plaqueIma)
    { 
       std::cout<<"Constructeur 4 arguments"<<std::endl;
        this->vitesse=vitesse;       
    }

    Voiture(std::string marque,  std::string couleur, std::string plaqueIma, std::shared_ptr<Personne> proprietaire) :Voiture(marque,couleur,plaqueIma)
    {
        this->proprietaire=proprietaire;
    }

    Voiture(std::string marque,  std::string couleur, std::string plaqueIma,int vitesse, int puissanceMoteur) :Voiture(marque,couleur,plaqueIma,vitesse)
    {
        moteur=std::make_unique<Moteur>(puissanceMoteur);
    }

    Voiture(const Voiture &v);

    // C++98
    //  private:
    // Voiture(const Voiture &v);

    // C++11
    // Voiture(const Voiture &v) =delete; 

    ~Voiture();

    std::string getMarque() const
    {
        return marque;
    }
        
    std::string getCouleur() const
    {
        return couleur;
    }

    void setCouleur(std::string couleur)
    {
        this->couleur=couleur;
    }

    std::string getPlaqueIma() const
    {
        return plaqueIma;
    }

    void setPlaqueIma(std::string couleur)
    {
        this->plaqueIma=plaqueIma;
    }
    
    int getVitesse() const
   {
       return vitesse;
   }
   
   int getCompteurKM()const
   {
       return compteurKm;
   }

    std::shared_ptr<Personne> getProprietaire() const
    {
        return proprietaire;
    }

    void setProprietaire(std::shared_ptr<Personne> proprietaire)
    {
        this->proprietaire=proprietaire;
    }

   static int  getCompteurVoiture()
   {
       return compteurVoiture;
   }



public:
    void accelerer(int vAcc);
    void freiner(int vFrn);
    void arreter();
    bool estArreter() const;

    void afficher() const; 
 
    static void testVarClasse();
    static bool egalVitesse(Voiture &v1,const Voiture &v2);
};