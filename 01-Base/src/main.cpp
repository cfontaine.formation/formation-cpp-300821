#include <iostream>
#include <bitset>
using namespace std;

// Variable globale
int varGlobal = 100;
int main()
{
	// Déclarer une variable
	int i;

	// Initialiser une variable
	i = 42;

	// Déclaration et initialisation
	double d = 12.3;

	cout << i << " " << d << endl;

	// Déclaration  multiple
	int mb, mc = 12;
	mb = 23;

	// Déclarer et initialiser une variable
	int j = 10;			// en C
	int jcpp98(10);		// en C++
	int jcpp11{ 10 };		// en C++11

	// Littéral entier => par défaut de type int
	long l = 12345L;		 // litéral long => L
	unsigned int u = 11U;	 // litéral unsigned => U
	long long lli = 100LL;	 // litéral long long => LL en C++ 11

	// Changement de base
	int decimal = 42;	// base par défaut: décimal
	int hexa = 0xFF;	// héxadécimal => 0x
	int octal = 0321;	// octal => 0
	int bin = 0b100101; // binaire => 0b en C++14

	// Avec cout
	// Manipulateur hex => affiche les valeurs numérique qui suivent en héxadécimal
	// Manipulateur oct => affiche les valeurs numérique qqui suivent en octal
	// Manipulateur dec => affiche les valeurs numérique qui suivent en décimal
	// Il n'y a de manipulateur pour les nombres binaires on peut utiliser un objet bitset
	cout << decimal << " " << hex << hexa << " " << oct << octal << " " << bitset<32>(bin) << dec << endl;

	// Littéral virgule flottante
	double d1 = 1.5;
	double d2 = 5.;
	double d3 = .5;
	double d4 = 1.32e3;

	// Littéral virgule flottante => double
	float f = 1.2F;
	long double ld = 1234.5L;

	// Littéral booléen
	bool b = true; // false

	// Littéral caractère
	char c = 'a';
	char cHexa = '\x41';	// caractère en hexadécimal
	char cOctal = '\32';	// caractère en octal
	cout << c << " " << cHexa << " " << cOctal << endl;

	// Typage implicite c++11
	// Le type de la variable est définie à partir de l'expression d'initialisation
	auto implicite = 34L;  // implicite est de type long
	auto implicite2 = 1.2; // implicite2 est de type double

	// Attention aux chaine de caratères: avec auto le type sera const char* et pas std::string
	auto impliciteSTR = "hello";

	// decltype  C++ 11 = > déclarer qu'une variable est de même type qu'une autre
	decltype(c) implicite3; // implicite3 est de type char comme c

   // Constante
	const double PIV = 3.14;
	//  PI=2;     // Erreur: on ne peut pas modifier une constante

	constexpr double PI2= 2*PIVAL;

	// Variable globale
	cout << varGlobal << endl;  // affiche 100
	// On déclare une variable locale qui a le même le nom que la variable globale
	int varGlobal = 2;

	// La variable globale est masquée par la variable locale
	cout << varGlobal << endl;  // affiche 2
	// Pour accéder à la variable globale, on utilise l’opérateur de résolution de portée ::
	cout << ::varGlobal << endl;  // affiche 100

	 // Opérateur
	// Opérateur arithmétique
	int aa = 1;
	int bb = 3;
	int res = aa + bb;
	cout << res << "  " << bb % 2 << endl; // % => modulo (reste de division entière) uniquement avec des entiers positif

	// Pré-incrémentation
	int inc = 0;
	res = ++inc;
	cout << res << " " << inc << endl; //inc=1 res=1

	// Post-incrémentation
	inc = 0;
	res = inc++;
	cout << res << " " << inc << endl; //inc=1 res=0

	// Affection composée
	res += 10; // correspond à res=res+10;
	res *= 2;  // correspond à res=res*2;

	// Opérateur de comparaison
	int val;
	cin >> val;
	bool tst = val > 10; // Une comparaison a pour résultat un booléen
	cout << tst << endl;

	// Opérateur logiques
	// non
	cout << tst << " " << !tst << endl;

	// Opérateur court-circuit && et || (évaluation garantie de gauche à droite)
	// && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
	// || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
	int o = 10;
	int p = 34;
	bool tst2 = o > 30 && p < 50; // comme o > 30 est faux,  p<50 n'est pas évalué
	bool tst3 = o == 10 || p > 100;
	cout << tst2 << " " << tst3 << endl;

	// Opérateur binaire (uniquement avec des entiers)
	// Pour les opérations sur des binaires, on utilise les entiers non signé
	unsigned int b1 = 0b101011;
	cout << bitset<32>(~b1) << endl;        // Complémént: 1 -> 0 et 0 -> 1
	cout << bitset<32>(b1 & 0b10) << endl;  // Et bit à bit          010
	cout << bitset<32>(b1 | 0b10) << endl;  // Ou bit à bit          101011
	cout << bitset<32>(b1 ^ 0b10) << endl;  // Ou exclusif bit à bit 101001

	cout << bitset<32>(b1 >> 2) << endl;    // Décalage à droite de 2 1010
	cout << bitset<32>(b1 << 2) << endl;    // Décalage à gauche de 2 10101100
	cout << (32 >> 1) << endl;              // Décalage à droite de 1 10000 correspond à une division par 2 
	cout << (32 << 1) << endl;              // Décalage à gauche de 1 1000000  correspond à une multiplication par 2 

	// Opérateur sizeof
	cout << sizeof(inc) << endl;    // nombre d'octets de la variable inc => 4
	cout << sizeof(double) << endl; // nombre d'octets du type double => 8 octets

	// Opérateur séquentiel , toujours évalué de gauche -> droite
	inc = 0;
	res = (inc++, o + p);                // inc++ est exécuté et l'expression retourne o+p
	cout << inc << " " << res << endl;   // inc = 1 res = 44

	// Conversion implicite => rang inférieur vers un rang supérieur (pas de perte de donnée)
	int convI = 12;
	double convD = 23.5;
	double convRes = convI + convD;

	// Promotion numérique => short, bool ou char dans une expression => convertie automatiquement en int
	short s1 = 2;
	short s2 = 3;
	int res2 = s1 + s2;

	// entier convertion en boolean
	bool convB1 = 34; // true
	bool convB2 = 0;  // false

	// boolean convertion en entier
	int convI1 = true;  //1
	int convI2 = false; //0

	// convertion explicite
	int te = 11;
	int div = 2;
	cout << (te / div) << endl;                      //5
	cout << (((double)te) / div) << endl;            //5.5 // en C
	cout << (static_cast<double>(te) / div) << endl; // 5.5 // en C++ -> le compilateur fait plus de vérification (sur le type)

	// Opérateur d'affectation conversion systémathique (implicite et explicite)
	double affD = 34.5;
	int affI = affD; // 34

	// Dépassement de capacité
	int dep = 300;
	char cDep = dep;           // 0001 0010 1100	300 => 300 > 127 ou 255, la valeur maximal d'un char
	cout << (int)cDep << endl; //      0010 1100	 44 => dans cDep, on n'a que le premier octet de de

	return 0;
}

