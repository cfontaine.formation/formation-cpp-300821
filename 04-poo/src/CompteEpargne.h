#pragma once
#include "CompteBancaire.h"

class CompteEpargne : public CompteBancaire
{
	double taux;

public:
	CompteEpargne(std::shared_ptr<Personne> titulaire, double solde, double taux) : CompteBancaire(solde,titulaire), taux(taux)
	{
	}

	void calculInterets();

};

