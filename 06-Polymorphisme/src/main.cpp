#include "Animal.h"
#include "Chien.h"
#include "Chat.h"
#include "Refuge.h"
#include <iostream>

using namespace std;
int main()
{
    /*Animal *a (new Animal(3,6000));
    a->afficher();
    a->emettreSon();*/

    Chien *c1=new Chien(4,4000,"Laika");
    c1->emettreSon(); 
    
    Animal *a2(new Chien(7,7000,"Rolo"));
    a2->emettreSon();

    // Chien *c2=(Chien*) a2;
    Chien *c2=dynamic_cast<Chien*>(a2);
    if(c2)
    {
        c2->emettreSon();
    }

    Refuge r;
    r.ajouter(new Chien(4,4000,"Laika"));
    r.ajouter(new Chat(5,4000,9) );
    r.ajouter(new Chien(7,7000,"Rolo"));
    r.ajouter(new Chat(4,3000,9) );
    r.ecouter();

   // delete a;
    delete c1;
    delete a2;
    return 0;
} 