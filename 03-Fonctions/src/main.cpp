#include <iostream>
#include "MesFonctions.h"

using namespace std;

const int ctsGlobal=67890;
static double varGlobal=3.14;   // avec static la variable n'est visible que dans ce fichier

// Déclaration de la fonction => Déplacer dans MesFonctions.h

// Déclaration d'un fonction inline, on place le code de la fonction dans le fichier.h
// inline => permet de définir des fonctions qui seront directement évaluées à la compilation
inline double doubler(double val);

int main(int argc, char* argv[])
{
    // Paramètre passé par valeur
	// C'est une copie de la valeur du paramètre qui est transmise à la méthode
    testParam(1);

    // Paramètre passé par adresse
	// La valeur de la variable passé en paramètre est modifié, si elle est modifiée dans la méthode
    int i=7;
    testAdresse(&i);
    cout<<i<<endl;

    // Pointeur de pointeur
    int* ptr;
    testAdresse2(&ptr);
    cout<<*ptr<<endl;
    delete ptr;

    // /!\ une fonction ne doit pas retourner une réfrence ou un pointeur sur une variable locale une fonction
    int* r= testRetourPointeur();
    cout<<*r<<endl;
    
    // Paramètre passé par référence
    int j=0;
    testreference(j);
    cout<<j<<endl;
    testreference2(j);
    // avec une référence constante, on peut aussi passer des litéraux
    testreference2(789);
    testreference2();

    const char *strEx1="azerty";
    cout<<estPresent(strEx1,6,'a')<<" "<<estPresent(strEx1,6,'C')<<endl;

    // Fonction à nombre variable de paramètre  (hérité du C)
    double moy=moyenne(4,5,5,7,8);
    cout<<moy<<endl;

    // Fonction à nombre variable de paramètre  (C++11)
    moy=moyenneCpp11({1,3,5});
    cout<<moy<<endl;

    // Fonction inline
    // L'appel d'une fonction iniline peut-être remplacé par la valeur évaluée lors de la compilation
    cout<<doubler(4)<<endl; // remplacer par d=12;
    double v;
    cin>>v; // ne peut pas être remplacé à la compilation, la fonction se comporte comme une fonction normale
    cout<<doubler(v)<<endl;

    // Fonction recursive
    factorial(3);

    // Paramètre de la fonction main
    for(int i=0;i<argc;i++)
    {
        cout<<argv[i]<<endl;
    }

    // Pointeur de fonction
    afficherOperation(3.5,4.7,somme);
    afficherOperation(3.5,4.7,multiplier);

    // Exercice  fonction tableau
    menu();

    // Classe de mémorisation
	// Static
    testMemorisation();
    testMemorisation();
    testMemorisation();
    // Extern
    testExtern();
    cout<<varGlobal<<endl;

    // Chaine de caractres C
    const char * cStr="Hello";
    //cStr[0]='h';
    cout<<cStr<<endl;

    char cStr2[]="Hello";
    cStr2[0]='h';
    cout<<cStr2<<endl;

    // string => en C++
    string str="Hello";
    string str2=string ("bonjour");
    string str3= string(7,'A'); 

    str+=" World";

    // Accès à un cararactère de la chaine [] ou at
    cout<<str[2]<<endl;
    cout<<str.at(2)<<endl; 

    //cout<<str[20]<<endl; // erreur
    //cout<<str.at(20)<<endl; // avec at si l'indice dépasse la taille de la chaine une exception est lancée

     // Extraire une sous-chaine
    cout<< str.substr(5)<<endl;     // substr => retourne une  sous-chaine qui commence à l'indice 5 et jusqu'à la fin
    cout<< str.substr(6,5)<<endl;   // substr => retourne une  sous-chaine de 5 caractères qui commence à l'indice 6

    str.insert(5,"-----");  // Insère la chaine passé en paramètre à la position 5
    cout<<str<<endl;

    str.erase(5,5); // Supprimer 5 caractères de la chaine à partir de la position 5
    cout<<str<<endl;

     // Recherche de la position de la chaine ou du caractère passée en paramètre dans la chaine str
    cout<<str.find("World")<<endl;      // à partir du début de la chaine
    cout<<str.find('o')<<endl;          // à partir du début de la chaine
    cout<<str.find('o',5)<<endl;        // à partir de la position 5
    // à partir de la position 8, pas de caractère => retourne la valeur std::string::npos
    cout<<str.find('o',8)<<" "<<string::npos<<endl;

    cout << str.rfind('o') << endl; // rfind => idem find, mais on commence à partir de la fin de la chaine et on va vers le debut
    cout<<str.rfind('o',6)<<endl;
    cout<<str.rfind('o',3)<<" "<<string::npos<<endl;

    const char * strCnv=str.c_str();    // convertion string en chaine C (const *char)
    cout<<strCnv<<endl;

    // Iterateur = > objet qui permet de parcourir la chaine(un peu comme un pointeur)
    // begin => retourne un iterateur sur le début de la chaine
    // end => retourne un iterateur sur la fin de la chaine
    for(string::iterator it= str.begin();it!=str.end();it++)    // it++ permet de passer au caractère suivant
    {
        cout<<*it<<endl;    // *it permet d'obtenir le caractère "pointer" par l'itérateur
       // *it='_'; //OK     // on a accés en lecture et en écriture
    }

    // rbegin et rend idem  mais l'itérateur part de la fin et va vers le début de la chaine
    for(string::reverse_iterator it=str.rbegin(); it!=str.rend();it++ )
    {
        cout<<*it<<endl;
    }

    // Différence rbegin / crbegin
    // crbegin (C++11) => on a accès uniquement en lecture par l'intermédiaire de l'ittérateur
    for(auto it= str.cbegin();it!=str.cend();it++)
    {
        cout<<*it<<endl;
       // *it='_'; // iterateur constant erreur
    }

    for(auto c :str) // C++11
    {
        cout<<c<<endl;
        c='-';
    }
 
    for(auto &c :str) // C++11
    {
        cout<<c<<endl;
        c='-';
    }
    for(auto c :str) // C++11
    {
        cout<<c<<endl;
    }

    str.clear();                 // Efface les caractères contenus dans la chaine
    cout << str.empty() << endl; // empty => retourne true si la chaine est vide

    // Exercice
	cout << inverser("Bonjour") << endl;
	cout << palindrome("Bonjour") << endl;
	cout << palindrome("radar") << endl;
	cout << acronyme("Comité international olympique") << endl;
	cout << acronyme("Organisation du traité de l'Atlantique Nord");

    return 0;
}

double doubler(double val)
{
	return 2.0 * val;
}

// Définition de la fonction => Déplacer dans MesFonctions.cpp

