#pragma once
#include<string>
#include<memory>
#include "Adresse.h"
class Personne
{
    std::string prenom;
    std::string nom;
    std::shared_ptr<Adresse> adresse;

    public:
    Personne(std::string prenom, std::string nom,std::shared_ptr<Adresse> adresse) : prenom(prenom), nom(nom), adresse(adresse)
    {
    }

    ~Personne();

    std::string getPrenom() const
    {
        return prenom;
    }

    void setPrenom(std::string prenom)
    {
        this->prenom=prenom;
    }

     std::string getNom() const
    {
        return nom;
    }

    void setNom(std::string nom)
    {
        this->nom=nom;
    }


    std::shared_ptr<Adresse> getAdresse() const
    {
        return adresse;
    }

    void setAdresse(std::shared_ptr<Adresse> adresse)
    {
        this->adresse=adresse;
    } 

    void afficher() const ;

};