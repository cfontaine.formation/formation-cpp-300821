#pragma once

#include "Animal.h"
#include <string>

class Chien : public Animal
{
    std::string nom;
    public:
    Chien(int age,int poid, std::string nom) : Animal(age,poid), nom(nom){}

    std::string getNom() const
    {
        return nom;
    }

    void emettreSon();
};