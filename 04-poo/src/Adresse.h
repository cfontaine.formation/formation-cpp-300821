#pragma once
#include<string>

class Adresse
{
    std::string rue;

    std::string ville;

    std::string codePostal;

public:
    Adresse(std::string rue,  std::string ville, std::string codePostal) : rue(rue),ville(ville),codePostal(codePostal){}

    std::string getRue() const
    {
        return rue;
    }

    void setRue(std::string rue)
    {
        this->rue=rue;
    }

    std::string getVille() const
    {
        return ville;
    }

    void setVille(std::string ville)
    {
        this->ville=ville;
    }

    std::string getCodePostal() const
    {
        return codePostal;
    }

    void setCodePostal(std::string codePostal)
    {
        this->codePostal=codePostal;
    }

    void afficher() const;

};