#include "Voiture.h"
#include <iostream>
 using namespace std;

int Voiture::compteurVoiture=0;

/*
Voiture::Voiture( std::string marque,  std::string couleur, std::string plaqueIma)
{
    this->marque=marque;
    this->couleur=couleur;
    this->plaqueIma=plaqueIma;
    //vitesse=0; //C++98
    //compteurKm=0;
}*/

Voiture::Voiture(const Voiture &v)
{
    cout<<"Constructeur par copie"<<endl;
    marque=v.marque;
    couleur=v.couleur;
    plaqueIma=v.plaqueIma;
    vitesse=v.vitesse;
    compteurKm=v.compteurKm;
    compteurVoiture++;
    proprietaire=proprietaire;
  //  moteur= new Moteur(*(v.moteur));
    moteur=make_unique<Moteur>(*v.moteur);
}

Voiture::~Voiture()
{
   // delete moteur;
    cout<<"Destructeur"<<endl;
}

void Voiture::accelerer(int vAcc)
{
    if(vAcc>0){
        vitesse+=vAcc;
    }
}

void Voiture::freiner(int vAcc)
{
    if(vAcc>0){
        vitesse-=vAcc;
    }
}

void Voiture::arreter()
{
    vitesse=0;
}

bool Voiture::estArreter() const
{
    return vitesse==0;
}

void Voiture::afficher() const
{
    cout<<"["<<marque<<" "<<couleur<<" "<<plaqueIma<<" "<<vitesse<<" "<<compteurKm<<endl;
    if(proprietaire)
    {
        proprietaire->afficher();
    }
    if(moteur)
    {
        moteur->afficher();
    }
}

void Voiture::testVarClasse()
{
    cout<<"methode de classe"<<endl;
    cout<<compteurVoiture<<endl;
    //cout<<vitesse<<endl;
    //arreter();
}


bool egalVitesse(Voiture &v1,const Voiture &v2)
{
    //v1.vitesse=v2.getVitesse();
    return v1.getVitesse()==v2.getVitesse();
}